#Praktikum PBO Pertemuan 4

#Source Code

```
import java.util.ArrayList;
import java.util.Scanner;

abstract class User {
    private String username;
    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public abstract void displayInfo();
}

class Admin extends User {
    public Admin(String username, String password) {
        super(username, password);
    }

    @Override
    public void displayInfo() {
        System.out.println("Admin account");
    }
}

class Customer extends User {
    private String name;
    private int age;

    public Customer(String username, String password, String name, int age) {
        super(username, password);
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    @Override
    public void displayInfo() {
        System.out.println("Customer account");
        System.out.println("Name: " + getName());
        System.out.println("Age: " + getAge());
    }
}

class Dashboard {
    private ArrayList<User> users;

    public Dashboard() {
        this.users = new ArrayList<User>();
    }

    public void addUser(User user) {
        this.users.add(user);
    }

    public void removeUser(User user) {
        this.users.remove(user);
    }

    public boolean loginUser(String username, String password) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public void displayUsers() {
        System.out.println("List of users:");
        for (User user : users) {
            user.displayInfo();
            System.out.println("Username: " + user.getUsername());
            System.out.println("Password: " + user.getPassword());
        }
    }
}

public class Log_in {
    public static void main(String[] args) {
        Dashboard dashboard = new Dashboard();

        Admin admin = new Admin("admin", "admin");
        Customer customer = new Customer("customer", "customer", "John", 30);

        dashboard.addUser(admin);
        dashboard.addUser(customer);

        Scanner input = new Scanner(System.in);

        System.out.print("Enter username: ");
        String username = input.nextLine();
        System.out.print("Enter password: ");
        String password = input.nextLine();

        if (dashboard.loginUser(username, password)) {
            System.out.println("Login successful!");
            dashboard.displayUsers();
        } else {
            System.out.println("Login failed!");
        }
    }
}


```

